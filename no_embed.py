from autogluon.tabular import TabularDataset, TabularPredictor

import numpy as np
import pandas as pd


c = ['k_{0}'.format(s) for s in range(68)]

train_data = pd.read_csv('handball_X_train.csv', low_memory=False)

label = 'state'
train_data[label] = pd.read_csv('handball_y_train.csv', header=None, names=['state'])
print(train_data.head())
print(train_data.describe())

time_limit = 60*60*6  # for quick demonstration only, you should set this to longest time you are willing to wait (in seconds)
metric = 'accuracy' # specify your evaluation metric here
predictor = TabularPredictor(label, eval_metric=metric).fit(train_data, presets='medium_quality_faster_train',hyperparameters={'NN':{}, 'GBM':{}, 'CAT':{}, 'RF':{}, 'XT':{}, 'KNN':{}})

test_data = pd.read_csv('handball_X_test.csv', low_memory=False)

y_pred = predictor.predict(test_data)
#y_test = TabularDataset('y_pred_test.csv')
y_test = pd.read_csv('handball_y_test.csv', header=None, names=['state'])['state']

perf = predictor.evaluate_predictions(y_true=y_test, y_pred=y_pred, auxiliary_metrics=True, detailed_report=True)

test_data[label] = y_test

lead = predictor.leaderboard(test_data, extra_info=True, extra_metrics=['accuracy', 'precision_micro', 'precision_weighted', 'recall_micro', 'recall_weighted'], silent=True)

lead.to_csv('lead.csv', index=False)

for m in predictor.get_model_names():
	print(m)
	y_pred = predictor.predict(test_data, model=m)

	perf = predictor.evaluate_predictions(y_true=y_test, y_pred=y_pred, auxiliary_metrics=True, detailed_report=True)
