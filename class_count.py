import numpy as np
import pandas as pd

train = pd.read_csv('handball_y_train.csv', header=None, names=['state'])
test = pd.read_csv('handball_y_test.csv', header=None, names=['state'])

print(train.describe())
print(test.describe())

data = pd.concat(
	[train, test], 
	axis=0, 
	ignore_index=True
)

print(data.value_counts())
print(data.size)